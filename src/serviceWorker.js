const staticDevGumi = "dev-gumi-site-v1"
const assets = [
  "index.html",
  "sb-admin-2.css",
  "js/app.js",
  "img2/undraw_posting_photo.png"
  "img2/undraw_profile.png"
  "img2/undraw_profile_1.png"
  "img2/undraw_profile_2.png"
  "img2/undraw_profile_3.png"
  "img2/undraw_rocket.png"

]
self.addEventListener("install", installEvent => {
  installEvent.waitUntil(
    caches.open{staticDevGumi}.them(cache => {
      cache.addAll(assets)
    })
  )
}
)
